class ArtworksController < ApplicationController
  before_action :set_artwork, only: [:show, :update, :destroy]

  # GET /artworks
  def index
    @artworks = Artwork.all

    render json: @artworks, methods: :medium_list, include: :artist
  end

  # GET /artworks/1
  def show
    render json: @artwork, methods: :medium_list, include: :artist
  end

  # POST /artworks
  def create
    @artwork = Artwork.new(artwork_params)
    @artwork.medium_list = params[:medium_list]
    if @artwork.save
      render json: @artwork, status: :created, location: @artwork
    else
      render json: @artwork.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /artworks/1
  def update
    if @artwork.update(artwork_params)
      @artwork.medium_list = params[:medium_list]
      @artwork.save
      render json: @artwork
    else
      render json: @artwork.errors, status: :unprocessable_entity
    end
  end

  # DELETE /artworks/1
  def destroy
    @artwork.destroy
  end

  # GET /artworks/mediums
  def mediums
    render json: ActsAsTaggableOn::Tag.all.map(&:name)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_artwork
      @artwork = Artwork.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def artwork_params
      params.require(:artwork).permit(:title, :image, :artist_id, :date, :medium_list, :active)
    end
end
