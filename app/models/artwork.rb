class Artwork < ApplicationRecord
  belongs_to :artist
  acts_as_taggable_on :mediums
end
