class CreateArtworks < ActiveRecord::Migration[5.1]
  def change
    create_table :artworks do |t|
      t.string :title
      t.string :image
      t.references :artist
      t.date :date
      t.boolean :active

      t.timestamps
    end
  end
end
